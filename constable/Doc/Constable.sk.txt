
	Constable - Autoriza�n� server projektu Medusa DS9

  Constable je autoriza�n� server pre bezpe�nostn� syst�m Medusa DS9.
Je vyv�jan� v r�mci projektu Medusa DS9 a je navrhnut� tak, aby v �o naj�ir�ej
miere umo��oval vyu�i� vlastnosti syst�mu Medusa DS9. S��asne bol bran� oh�ad
na �o najpohodlnej�iu konfigur�cia. Konfigur�cia bezpe�nostn�ho syst�mu v�ak
nikdy nem��e by� trivi�lna z�le�itos�, lebo vy�aduje d�kladn� znalosti dan�ho
opera�n�ho syst�mu a aplik�ci�, ktor� na �om be�ia. Proces konfigur�cie je
�asto spojen� s mnoh�mi, �asto ne�spe�n�mi pokusmi. Najv���m probl�mom je
fakt, �e be�ne pou��van� opera�n� syst�my a aplik�cie nad nimi s� navrhovan�
nanajn�� s oh�adom na zabezpe�enie, ktor� pon�ka samotn� opera�n� syst�m.
V praxi to vyzer� tak, �e niektor� "neposlu�n�" aplik�cie vy�aduj� dos� �irok�
mno�inu pr�v a s� dos� h�klive na ich obmedzenie.


	1. Konfigur�cia

  Ke�e samotn� Constable je dos� komplexn� program, je potrebn� okrem
konfigur�cie bezpe�nost�ho syst�mu Medusa DS9 konfigurova� aj samotn�ho
Constabla. Existuj� teda dve konfigur�cie:
		1) konfigur�cia Constabla
		2) konfigur�cia syst�mu Medusa DS9


	1.1. Konfigur�cia Constabla

  Autoriza�n� server Constable je schopn� robi� naraz autoriza�n� server pre
viacero opera�n�ch syst�mov. Toto je hlavn�m d�vodom nutnosti jeho konfigur�cie.
Konfigura�n� s�bor (zvy�ajne nazvan� constable.conf) m� n�sledovn� syntax:
    - pozn�mky s� v�etko medzi symbolmi '/*' a '*/'
    - ako pozn�mka sa ch�pe aj obsah riadku po symbole '//', alebo '#'.
    - jednotliv� konfigura�n� pr�kazy sa odde�uj� bodko�iarkou ';'.
    - pr�kazy:
	chdir "cesta"  - nastavi pracovn� adres�r Constabla pod�a zadanej cesty.
	system "pr�kaz"- pred samotn�m �tartom Constabla sa vykon� shellovsk�
			 pr�kaz.
	config "cesta" - ur�� cestu ku konfigura�n�mu s�boru pre bezpe�nostn�
			 syst�m Medusa DS9.
	module "n�zov" [file "cesta"] - ur��, �e dan� modul bude pou�it�. Pre
			 niektor� moduly je mo�n� definova� cestu k ich
			 konfigura�n�mu s�boru.
    - defin�cie spojen� so syst�mami na autoriz�ciu. Za��naj� v�dy n�zvom
      spojenia v umodzovk�ch. Dalej n�sleduje typ spojenia a parametre. Mo�n�
      typy spojen� s�:
	"n�zov" file "cesta" - spojenie prostredn�ctvom �peci�lneho s�boru
			 ur�en�ho cestou.
	"n�zov" tcp:<port> <ip>[/<mask>][:<port>] - spojenie prostredn�ctvom
			 tcp spojenia. Constable je v�dy pasivnou stranou
			 spojenia, teda cak� na nadviazanie spojenia od
			 syst�mu. <port> je ��slo portu na ktorom na spojenie
			 �ak�. <ip>[/<mask>][:<port>] je ip adresa, pr�padne
			 aj maska a port, z ktorej je mo�n� sa na Constabla
			 pripoji�. Port na ktorom Constable �ak� sa m��e
			 opakova� pre viacero spojen�. Musia by� v�ak
			 odl��iteln� inform�ciami o druhej strane.


	1.2. Konfigur�cia syst�mu Medusa DS9

  Pred touto kapitolou si pros�m pre��tajte dokument�ciu k samotnej Meduse
(Medusa.txt). N�jdete tam vysvetlen� z�kladn� pojmy ako k-objekty, VS, atd ...
Konfigur�cia syst�mu Medusa DS9 logicky pozost�va z nieko�k�ch �ast�:

	- defin�cia stromov K-objektov.

	- defin�cia priestorov (spaces).

	- ur�enie pr�v medzi priestormi.

	- defin�cia obslu�n�ch funkci� udalost� sledovan�ch v syst�me.

	- pomocn� funkcie

	- �peci�lne pomocn� funkcie


	1.2.1. Defin�cia stromov K-objektov

  Stromy k-objektov definuj� priestor mien (namespace). ��elom pristoru mien je
stanovi� syst�m pomen�vania, teda identifik�cie objektov. Ka�d� strom je stromom
pre jeden typ k-objektov (napr�klad file, process, ...). Pre jeden typ
k-objektov m��e by� definovan�ch viacero stromov. Ich po�et je limitovan�
ve�kos�ou po�a 'cinfo' pre tento typ k-objektu. Defaultn� hodnoty s�
1 pre k-objetky vystupuj�ce v�hradne ako objekty oper�ci� a 4 pre k-objekty,
ktor� m��u vystupova� aj ako subjekty oper�ci�.

  Ka�d� k-objekt sa nach�dza v�dy v ka�dom strome pre jeho typ. A v ka�dom
z t�chto stromov pr�ve na jednom mieste.

  Ka�d� strom je pomenovan� menom. Toto meno sa nach�dza v�dy na prvom mieste
v ka�dej ceste. Cesta v strome je teda zlo�en� z n�zvu stromu a nazvov
jednotliv�ch uzlov na ceste od kore�a stromu a� k uzlu ktor� pomen�va. Men�
uzlov v ceste s� oddelen� znakom '/'.

  Usporiad�vanie k-objektov do stromu umo�nuje aj ist� automatiz�ciu, ktor�
je mo�n� ur�i� pri defin�cii stromu. Je to zjednodu�enie explicitn�ho ur�ovania
ka�d�mu objektu jeho miesto v stromoch. V niektor�ch pr�padoch je v�ak
v�hodnej�ie toto explicitn� ur�ovanie.

  Syntax defin�cie stromu:

	tree "n�zov" [<opt>] of <class> [by <op> <exp>] ;

  "n�zov"  predstavuje n�zov stromu.
  <opt>    m��e by� �iadn�m, jedn�m, alebo viacer�mi z n�sleduj�cich volieb
           oddelen�ch znakom '|':
	   	test_enter	- ur�uje, �e pri zara�ovan� k-objektu do stromu
				  bude kontrolovan� pr�vo 'ENTER' tohto
				  k-objektu na cie�ov� miesto.
		clone		- ur�uje, �e pri automatickom zara�ovan�
				  k-objektov do stromu sa uplatn� dedi�n�
				  princ�p, teda poz�cia v strome pre k-objekt
				  bude uzol hierarchicky pod uzlom v ktorom
				  sa nach�dza jeho rodi�ovsk� k-objekt.
				  V opa�nom pr�pade sa v�dy vych�dza
				  z kore�ov�ho uzla dan�ho stromu.
  <class>  predstavuje typ k-objektu pre ktor� je tento strom.
  <op>     je oper�cia pomocou ktorej sa k-objekty automaticky zaraduj� do
           stromu. Subjekt oper�cie musi by� k-objekt tak�ho ist�ho typy ako
	   strom.
  <exp>    v�raz, ktor�ho v�sledkom mus� by� re�azec predstavuj�ci n�zov
           poduzla do ktor�ho sa m� k-objekt, ktor� vystupuje ako subjekt
	   oper�cie, zaradi�. Re�azec by nemal obsahova� znaky '/', nako�ko
	   tie nie su platn� v n�zve uzla a automatick� zara�ovanie funguje len
	   na zara�ovanie do uzla bezprostredne pod kore�ov�m uzlom stromu,
	   alebo v pr�pade, �e bol strom definovan� ako 'clone' a objektom
	   sledovanej oper�cie je k-objekt tak�ho ist�ho typu ako strom, je
	   k-objekt subjektu oper�cie zaraden� do uzla bezprostredne pod uzlom
	   do ktor�ho je zaraden� k-objekt objektu oper�cie.

  Pre explicitn� zara�ovanie k-objektov do stromu vi� kapitola vstavan� pr�kazy
  
  Pr�klady defin�cie stromu (pre lep�ie pochopenie pozri popis jednotliv�ch
oper�ci�):

	tree "domain" of process;

	tree "uids" of process by setuid setresuid ""+setresuid.ruid;

	tree "exec" clone of process by pexec primaryspace(file,@"fs");

	tree "fs" clone of file by getfile getfile.filename;

  Jeden z definovan�ch sromov mo�e by� stanovan� ako prim�rny. Pri u�dzan�
cesty v tomto strome potom nie je nutn� uv�dza� n�zov tohto stromu. Ak cesta
za��na znakom '/' je implicitne pova�ovan� za cestu v prim�rnom strome.

  Prim�rny strom je ur�en� pr�kazom:

	primary tree "n�zov" ;


	1.2.2. Defin�cia priestorov (spaces)

  Priestory s� pomenovan� podmnoziny k-objektov. Definuj� sa ako cesty
v stromoch. Pr�slu�nos� k-objektu k priestoru je teda z�visl� od pomenovania
k-objektu na z�klade stanoven�ho priestoru mien. In�mi slovami na z�klade
poz�cie k-objektu v jeho strome je ur�en� jeho pr�slu�nos� k priestorom.
Priestory de facto zodpovedaj� virtualnemu svetu VS ako ho definuje
bezpe�nostn� syst�m Medusa DS9. Rozdiel je len v mo�nej optimaliz�cii zo strany
Constabla. V�sledkom m��e by�, �e nie ka�d�mu priestoru zodpoved� jeden VS
a tie� nie ka�d�mu VS zodpoved� jeden priestor.

  Constable umo�nuje pri defin�cii priestormu ur�i� nie len cesty, ktor� do�
patria, ale aj cel� podstromy. Navy�e umo��uje ur�i� cesty a podstromy, ktor�
do� nepatria. Cesty nemusia by� ur�en� len ako samostatn� cesty, ale je mo�n�
ur�i�, �e do dan�ho priestoru patria, alebo nepatria v�etky cesty in�ho
priestoru.

  Pr�slu�nos� v�etk�ch ciest a podstromov k priestoru nemus� by� definovan� na
jednom mieste v konfigura�nom s�bore, ale je mo�n� na viacer�ch miestach
v konfigura�nom s�bore prid�va� k priestoru cesty a podstromy, ktor� do� patria
a nepatria.

  Syntax:

	Deklar�cia priestoru
		[primary] space "n�zov" ;
	
	Defin�cia priestoru (deklar�cia je implicitn�)
		[primary] space "n�zov" = <p> [<+|-> <p> ...]

	Pridanie �al��ch ciest do priestoru
		space "n�zov" <+|-> <p> [<+|-> <p> ...]

	"n�zov" predstavuje n�zov priestoru.
	<+|-> je znak '+' alebo '-'.
	<p> m��e by� jedno z n�sleduj�cich:
		"cesta" - cesta v strome ur�uj�ca jeden konkr�tny uzol.
		recursive "cesta" - cesta v strome ur�uj�ca cel� podstrom pod
		                    uzlom, ktor� reprezentuje.
		space <space> - ur�uje v�etky cesty v priestore <space>. Pritom
		                nez�le�� na tom, �i s� cesty v priestore
				<space> definovan� pred, alebo a� po tomto
				pou�it�.
	
  Jeden uzol v strome nemus� patri� do �iadn�ho priestoru, alebo m��e patri�
do �ubovo�n�ho mno�stva priestorov.

  �peci�lnym pr�padom s� prim�rne priestory. Oproti normalnym priestorom sa
l��ia obmedzen�m, �e jeden uzol mo�e patri� nanajv�� do jedn�ho primarn�ho
priestoru. Prim�rne priestory v�ak d�vaj� mo�nos� pod�a zaradenia k-objektu do
stromu z�ska� n�zov jeho prim�rneho priestoru v ur�itom strome. Vi� vstavan�
funkcia primaryspace(kobject,path).

	
	1.2.3. Ur�enie pr�v medzi priestormi

  Jedn� sa o priradenie pr�v k-objektom patriacim do jedn�ho priestoru ku
k-objektom patriacim do druh�ho priestoru. Rozli�uje sa nieko�ko typov pr�v.
Jedn� sa o mo�n� typy pr�stupov k-objektu ako subjektu oper�cie ku k-objektu
ako objektu oper�cie. Mo�n� typy s� n�sledovn�:

	RECEIVE alebo READ	- ��tanie, alebo pr�jem inform�ci� z objektu
				  oper�cie
	SEND alebo WRITE	- z�pis, alebo posielanie inform�ci� do objektu
				  oper�cie
	SEE			- pr�vo vidie� objekt oper�cie
	CREATE			- pr�vo vytv�ra� objekt oper�cie
	ERASE			- pr�vo zru�i� objekt oper�cie
	ENTER			- pr�vo nadob�da� stav odpovenaj�ci objektu
				  oper�cie
	CONTROL			- pr�vo riadi� objekt oper�cie

  Nie v�etky typy pr�v s� podporovan� ka�d�m kernelom. Minim�lne v�ak musia by�
podporovan� prv� tri, teda: READ, WRITE a SEE. V Linuxe s� podporovan� iba tieto
tri. Constable v�ak podporuje v�etk�ch sedem a m��u by� vyu�it� modulmi
implementuj�cimi r�zne bezpe�nostn� politiky. Pr�vo ENTER m��e by� kontrolovan�
pri zara�ovan� k-objektu do stromu (automatickom aj explicitnom).

  Syntax priradenia pr�v:

	<space1> <typ> <space2> [, [<typ>] <space2> ... ]

  <space1> je priestor ktor�mu prira�ujeme pr�va.
  <typ> je typ prira�ovan�ho pr�va.
  <space2> je priestor nad ktor�m je povo�ovan� robi� oper�cie typu <typ>.

  Pozn�mka: Priradenie pr�v m� zmysel len pre k-objekty zo <space1>, ktor�
vystupuj� ako subjekty oper�cie, kde objektom oper�cie je k-objekt zo <space2>.


	1.2.4. Defin�cia obslu�n�ch funkci� udalost� sledovan�ch v syst�me

  Prim�rnym ��elom t�chto funkci� je umo�ni� zmenu stavu k-objektov, najm� ich
poz�ciu v strome, na z�klade nejakej udalosti v syst�me. Sekund�rnym ��elom je
umo�ni� rozhodnutie o poveln� nejakej oper�cie v syst�me na z�klade
�pecifickej��ch krit�rii.

  Funkcie s� programy p�san� v jazyku ve�mi podobnom jazyku C. V jazyku je
mo�n� okrem vyhodnocovania v�razov, ktor� plne, vr�tane prior�t, zodpoved�
jazyku C pou�i� aj riadiace kon�trukcie if-else, do-while-else, for,
switch-case, break a continue. K dispoz�cii je aj nieko�ko vstavan�ch pr�kazov,
ktor� umo�nuj� napr�klad explicitn� zaradenie k-objektu do stromu. Bli��ie
o tom pojedn�va kapitola programovac� jazyk.

  Ako �trukt�ry zn�me z jazyka C s� k dispoz�cii k-objekty subjektu a objektu
oper�cie. Samotn� oper�cia - sledovan� udalos� v syst�me je tie� pr�stupn�
ako �trukt�ra a m��e ma� svoje atrib�ty.

  �trukt�ra k-objektov ako aj oper�ci� a aj samotn� oper�cie s� definovan�
kernelom. Constable ich popis z�ska od kernelu. Kompletn� zoznam v�etk�ch
oper�ci� a k-objektov v syst�me, vr�tane ich �trukt�ry je mo�n� z�ska� spusten�m
Constabla s argumentom -D. Pr�klad:

	constable -D kobjekty.txt

  Po na��ta� a spracovan� konfigura�n�ho s�boru sa constable spoj� s kernelom,
ako definuje konfigura�n� s�bor constable.conf. Potom zap��e do s�boru
kobjekty.txt popis v�etk�ch k-objektov v syst�me a �alej pokra�uje vo svojej
norm�lnej �innosti.

  Vygenerovan� s�bor m� n�sleduj�ci form�t:

	REGISTER ["n�zov spojenia"] class <n�zov triedy k-objektu> {
		<d�tov� typ>	<n�zov atrib�tu> (<offset>:<d�ka>)
		...
	}

	REGISTER ["n�zov spojenia"] event <n�zov subjektu>:<typ subjektu>
	<n�zov oper�cia> <n�zov objektu>:<typ objektu> (<��slo oper�cie>)
	<sp�sob sledovania> {
		<d�tov� typ>	<n�zov atrib�tu> (<offset>:<d�ka>)
		...
	}

  V pr�pade, �e oper�cia nem� objekt oper�cie je na miesto
<n�zov objektu>:<typ objektu> znak '-'. <typ subjektu> a <typ objektu> s� n�zvy
tried (typov) k-objektov. <sp�sob sledovania> nesie dve inform�cie. Prv� je,
�i sa sledovanie tejto oper�cie zna�� pri subjekte, alebo objekte oper�cie.
Druh� je �i sa sledovanie zna�� do subjektov�ch, alebo objektov�ch eventov.
Ka�d� k-objekt m��e ma� z�rove� subjektov� aj objektov� eventy. Zvy�ajne ak
sa sledovanie zna�� subjektu, tak sa zna�� do jeho subjektv�ch eventov. Podobne
je so so zna�en�m sledovania oper�cie objektu. Existuj� v�ak v�nimky. Napr�klad
oper�cie getfile m� ako subjekt oper�cie k-objekt typu file a tento nem�
subjektov� eventy. Preto sa sledovanie tejto oper�cie zna�� do jeho objektov�ch
eventov.

  Syntaktikcy to vyzer� n�sledovne:

	to subject subject's event - zna�en� subjektu do subjektov�ch eventov
	to object object's event - zna�en� objektu do objektov�ch eventov
	to subject object's event - zna�en� subjektu do objektov�ch eventov
	to object subject's event - zna�en� objektu do subjektov�ch eventov

  D�le�it� je vedie� �i je sledovanie oper�cie zna�en� pri subjekte, alebo
objekte oper�cie. Toto je potrebn� ma� na zreteli pri p�san� obslu�n�ch funkcii
sledovan�ch oper�ci�, nako�ko sledovnie sa zap�na automaticky a je d�le�it�
aby ho bolo kde zapn��. Bude to objasnen� neskor�ie.

  Syntax defin�cie obslu�nej funkcie:

	<space1> <op> [:<ehh_list>] [<space2>] { <cmd> ... }

  <space1> je priestor subjektu oper�cie.
  <space2> je priestor objektu oper�cie. Ak oper�cia objekt nem�, neuv�dza sa.
  <op> je oper�cia.
  <ehh_list> ur�uje �i sa jedn� o rozhodovancie pravidlo, alebo reakciu na
udalos�, ktor� nastane, pr�padne nenastane. Ako <ehh_list> m��e by� uveden�
jeden zo zoznamov:
		VS_ALLOW	- oper�cia bola povolen� VS modelom v kerneli.
				  V tejto funkcii je mo�n� rozhodn�� o jej
				  povolen�, alebo zamietnut�.
		VS_DENY		- oper�cia bola zamietnut� VS modelom v kernali.
				  V tejto funkci� sa u� nam� robi� rozhodovanie.
				  Toto nemus� by� podporovan� kernelom.
		NOTIFY_ALLOW	- oper�cia pre�la schv�len�m v�etk�mi
				  relevantn�mi funkciami VS_ALLOW a v�sledok
				  je, �e oper�cia bude povolen�.
				  V tejto funkci� sa u� nemaj� robi�
				  rozhodnutia, ale iba reagova� na skuto�nos�,
				  �e oper�cia prebehne.
		NOTIFY_DENY	- oper�cia pre�la v�etk�mi relevantn�mi
				  funkciami VS_ALLOW alebo VS_DENY s v�sledkom,
				  �e oper�cia bude zamietnut�.
				  V tejto funkci� sa u� nemaj� robi�
				  rozhodnutia, ale iba reagova� na skuto�nos�,
				  �e oper�cia neprebehne.

  Ak <ehh_list> nie je uveden�, pova�uje sa za VS_ALLOW.

  <cmd> predstavuje program. Pre bli��ie vysvetlenie vi�. kapitola programovac�
jazyk. Vykon�vanie programu kon�� pr�kazom return(<v�raz>). Kde v�raz m� jednu
z hodn�t:
		ALLOW	- povoli� oper�ciu bez oh�adu na in� (unixov�) pr�va.
		DENY	- zak�za� oper�ciu.
		SKIP	- nevykona� oper�ciu, ale nereportova� chybu.
		OK	- prenecha� rozhodnutie na in� (unixov�) pr�va.

  Ak je v priebehu rozhodovania vykonan�ch viacero obslu�n�ch funkci� v�sledok
sa vypo��ta pod�a n�sleduj�cich pravidiel:
	- ak jedna z funkci� rozhodla DENY, plat� DENY
	- ak �iadna funkcia nerozhodla DENY, ale jedna rozhodla SKIP, plat� SKIP
	- ak �iadna funkcia nerozhodla DENY ani SKIP a jedna rozhodla ALLOW
	  plat� ALLOW
	- ak funkcie rozhodly len OK, plat� OK.

  Ak funkcia nekon�� pr�kazom return(), pova�uje sa �e skon�ila ako return(OK).

  Postup spracovania v�etk�ch obslu�n�ch funkci� je n�sledovny:
	1) Vykonaj� sa v�etky funkcie zo zoznamu VS_ALLOW, pr�padne VS_DENY.
	   Ich poradie nie je definovan�.
	2) Na z�klade v�sledkov jednotliv�ch funkci� sa vyhodnot� vysledn�
	   rozhodnutie. (vi�. vy��ie)
	3) Vykonaj� sa v�etky funkcie zo zoznamu NOTIFY_ALLOW, pr�padne
	   NOTIFY_DENY. Ich poradie nie je definovan�.

  Sledovanie oper�cie sa zap�na automaticky. Ak sa sledovanie oper�cie zna��
pri subjekte, bude pozna�en� pri ka�dom k-objekte priestoru <space1>. Ak sa
zna�� pri objekte, bude pozna�en� pri ka�dom k-objekte priestoru <space2>.
<space1> a <space2> m��u by� nahraden� znakom '*', ktor� znamen� ka�d� k-objekt.
V pr�pade, �e je '*' pou�it� na mieste priestoru, ktor�ho k-objektom sa zna��
sledovanie oper�cie, toto sa nestane a sledovanie oper�cie nebude nastaven�.
Preto by '*' nemala by� pou�it� na taktom mieste.
<space1> a <space2> je tie� mo�n� nahradi� cestou v strome nap�sanou
v uvodzovk�ch, ktor� m��e by� predch�dzan� k���ov�m slovom recursive. V�znam
je obdobn� defin�cii nov�ho priestoru a jeho pou�itiu iba na tomto jednom
mieste.


	1.2.5. Pomocn� funkcie

  V konfigura�nom s�bore je mo�n� definova� aj pomocn� funkcie, ktor� je potom
mo�n� vola� z obslu�n�ch funkci�, alebo in�ch pomocn�ch funkci�. V pr�pade, �e
je potrebn� funkciu pou�i� v konfigura�nom s�bore pred jej defin�ciou je
potrebn� najprv ju deklarova�.

  Syntax deklar�cie funkcie:

	function <n�zov> ;

  Syntax defin�cie funkcie:

	function <meno> { <cmd> ... }

  <cmd> predstavuje program. Pre bli��ie vysvetlenie vi�. kapitola programovac�
jazyk.

  Funkcia m��e by� volan� s argumentami. Argumenty funkcie s� vo funkci�
dostupn� pomocou symbolu '$' bezprostredne n�sledovan�ho poradov�m ��slom
argumentu. Argumenty s� ��slovan� od 1.

	1.2.6. �peci�lne pomocn� funkcie

  Niektor� pomocn� funkcie maj� �pecialny v�znam. Su definovan� ako oby�ajn�
pomocn� funkcie. Ke� s� v�ak definovan� s� automaticky zavolan� za ur�it�ch
okolnos�.

	1.2.6.1. function _init

  �peci�lna pomocn� funkcia _init, ak je definovan� je zavolan� automaticky
po spojen� sa s ka�d�m kernelom potom, ako je vykomunikovan� �truk�ra k-objektov
a oper�ci�. T�to funkcia je zavolan� predt�m ako je spracovan� prv� udalos�
od pr�slu�n�ho kernelu.

	1.3. Programovac� jazyk

  Programovac� jazyk je pou��van� v troch pr�padoch:
	- v obslu�n�ch funkci�ch
	- v pomocn�ch funkci�ch
	- pri deklar�cii stromu k-objektov s automatick�m zara�ovan�m. V tomto
	  pr�pade je v�ak k mo�n� pou�i� len jeden v�raz, �o v�ak v princ�pe
	  nijako nelimituje mo�nosti jazyka, lebo volanie funkcie je v�razom.

  Programovac� jazyk pozost�va z t�chto s��ast�:
	- ria�iace kon�trukcie
	- oper�tory a vyhodnocovanie v�razov
	- kon�tanty
	- premenn�
	- vstavan� funkcie


	1.3.1. Ria�iace kon�trukcie

  Pr�kazy sa vykon�vaju v porad� v akom su nap�san� (z �ava do prava, z hora
nadol). Pr�kazy s� oddelen� znakom ';'. Na mieste kde je o�ak�van� jeden pr�kaz
m��e by� blok za��naj�ci znakom '{' a kon�iaci znakom '}'. V tomto bloku m��e
by� �ubovo�n� mno�stvo pr�kazov, alebo vnoren�ch blokov. Ako pr�kaz je ch�pan�
�ubovo�n� v�raz, pr�kaz riadiacej kon�trukcie, alebo blok. Za blokom v�ak
nenasleduje odde�ova� pr�kazov ';'.

	1.3.1.1. if - else

  Riadiaca kon�trukcia if - else sl��i na vetvenie programu. Syntax je:

		if <v�raz>
			<pr�kaz1>
		[else
			<pr�kaz2>]

  Ak je logick� hodnota v�razu <v�raz> pravdiv� (nenulov�) vykon� sa <pr�kaz1>,
inak sa vykon� <pr�kaz2>, ak je uveden�.

	1.3.1.2. do - while - else

  Riadiaca kon�trukcia  do - while - else je zov�eobecnen� cyklus. Syntax je:

		[do
			<pr�kaz1>]
		while <v�raz>
			<pr�kaz2>
		[else
			<pr�kaz3>]

  Funkcionalita je obdobn� kon�trukci� if - else s t�m rozdielom, �e sa
vykon�vanie pr�kazov <pr�kaz1> a <pr�kaz2> opakuje. Cyklus m��e kon��� dvoma
sp�sobmi. Prv�m sp�sobom je ak v�raz <v�raz> je nepravdiv� (nulov�). V takom
pr�pade sa na z�ver vykon� <pr�kaz3> a cyklus skon��. Druh�m sp�sobom je
pou�itie pr�kazu break v cykle. Pou�itie pr�kazu break m� za n�sledok okam�it�
ukon�enie cyklu. <pr�kaz3> sa nevykon�va. Okrem pr�kazu break existuje pr�kaz
continue, ktor� pri pou�it� v tele cyklu m� za n�sledok preru�enie vykon�vania
cyklu a pokra�ovanie na vyhodnoten� v�razu <v�raz>, ��m cyklus pokra�uje nov�m
obehom, ale bez vykonania pr�kazu <pr�kaz1>. Pr�kazy break a continue m��u by�
pou�it� na mieste <pr�kaz1>, <pr�kaz2> alebo <pr�kaz3>.

	1.3.1.3. for

  Riadiaca kon�trukcia for je zjednodu�enou variantou cyklu. Syntax je:

		for(<v�raz1>;<v�raz2>;<v�raz3>)
			<pr�kaz>

  Tato kon�trukcia je funk�ne ekvivalentn� kon�trukcii:

		<v�raz1>;
		while( <v�raz2> )
		{
			<pr�kaz>;
			<v�raz3>;
		}

s t�m rozdielom, �e pr�kaz continue vykon� <v�raz3> pred vyhodnoten�m v�razu
<v�raz2>.

	1.3.1.4. switch - case

  Riadiaca kon�trukcia switch - case sl��y na vetvenie programu. Pr�klad:

		switch <v�raz>
		{
			case <v�raz1>:
				<pr�kaz1>
				...
			case <v�raz2>:
				<pr�kaz2>
				...
			...
			default:
				<pr�kaz3>
				...
		}

  Hodnota v�razu <v�raz> sa porovn�va so v�etk�mi v�razmi za k���ov�mi slovami
case a� pok�m sa nen�jde rovnos�. Ak sa napr�klad <v�raz> = <v�raz1> vykonaj�
sa pr�kazy <pr�kaz1>. Ak sa v�kon� pr�kaz break, vykon�vanie celej kon�trukcie
switch sa ukon��. V opa�nom pr�pade ak vykon�vanie programu dospeje k �al�iemu
case, alebo default (v na�om pr�klade k case <v�raz2>:) pokra�uje sa vo
vykon�van� �al��ch pr�kazov (v na�om pr�klade <pr�kaz2>). V pr�pade ak nebola
n�jden� �iada rovnos� s nijak�m case <v�razX> vykonaj� sa pr�kazy za k���ov�m
slovom default (<pr�kaz3>). Aj v tomto pr�pade ukon�uje pr�kaz break vykon�vanie
�al��ch pr�kazov. Kon�trukcia m��e obsahova� �ubovo�n� mno�stvo podmienok case
a m��e ale nemus� obsahova� default.

	1.3.2. Oper�tory a vyhodnocovanie v�razov

  Oper�tory a rekurz�vnos� usporiadan� pod�a priority:
	()		LP	- z�tvorky
	! ~ - ++ --	PL	- un�rne oper�tory:
				  logick� neg�cia, aritmetick� neg�cia,
				  z�porn� znamienko,
				  autoinkrement�cia, autodekrement�cia
	* / %		LP	- kr�t, deleno, modulo
	+ -		LP	- plus, m�nus
	<< >>		LP	- aritmetick� bitov� posuv do�ava, doprava
	< <= > >=	LP	- men��, men�� alebo rovn�, v���,
				  v��� alebo rovn�
	== !=		LP	- rovn�, nerovn�
	&		LP	- bitov� AND
	^		LP	- bitov� XOR
	|		LP	- bitov� OR
	&&		LP	- logick� AND
	^^		LP	- logick� XOR
	||		LP	- logick� OR
	?:		PL	- <v�raz1> ? <v�raz2> : <v�raz3>
				  ak plat� <v�raz1>, v�sledkom bude <v�raz2>,
				  inak bude v�sledkom <v�raz3>
	= += -= ...	PL	- priradenie, pripo��tanie, odpo��tanie, ...
				  += -= *= /= %= &= |= ^= <<= >>=
	,		LP	- �iarka. v�sledkom je oper�tor vpravo,
				  vyhodnotia sa v�ak v�etky.

  Pre logick� oper�tor AND (&&) plat� �e ak hodnota �avej strany je nepravdiv�,
potom sa prav� strana v�bec nevyhodnot�. Obdobne pre logick� oper�tor OR (||)
plat�, �e ak je jeho �av� strana pravdiv�, prava strana sa v�bec nevyhodnot�.

  Ako prav�, alebo �av� stranu oper�torov mo�no pou�i� v�raz, kon�tantu,
premenn�, alebo volanie vstavanej alebo pomocnej funkcie. �av� strana priradenia
mus� by� premenn�. Taktie� ur�it� oper�cie je mo�n� robi� iba medzi ur�it�mi
typmi.

	1.3.3. Kon�tanty

  Programovac� jazyk pozn� nieko�ko typov kon�tant:
	- ��seln� kon�tanty - m��u by� zapisovan� v desiatkovej, osmi�kovej,
		alebo �estn�stkovej s�stave. '0x' na za�iatku indikuje
		�estn�stkov� s�stavu. '0' na za�iatku indikuje osmi�kov�
		s�stavu. Inak je ��slo v desiatkovej s�stave.
	- textov� re�azce - uv�dzaj� sa v uvodzovk�ch '"'.
	- znakov� acsii hodnota - jeden ascii znak uveden� v apostr�foch '''
	- cesta v strome k-objektov - za��na znakmi '@"' a kon�� znakom '"'.
		pr�klad: @"fs/etc"
	- niektor� kon�tanty maj� svoje k�u�ov� slov�:
		- ALLOW, DENY, SKIP, OK
		- ��sla syscallov Linuxu (za��naj� ako sys_)
		- ��sla posix capabil�t z Linuxu (za��naj� ako CAP_)
	- ��sla priestorov dostupn� pomocou kon�trukcie:
		space <n�zov>
	  Tak�to kon�tanta m� hodnotu bitov�ho po�a s nastaven�m bitom
	  zodpovedaj�cim priestoru s n�zvom <n�zov>. Ostatn� bity s� vynulovan�.

	1.3.4. Premenn�

  Premenn� programovacieho jazyka s�:
	- k-objekty vystupuj�ce ako subjekty, alebo objekty oper�cie, pr�padne
	  samotn� oper�cia. S� dostupn� pod menami ako ich definuje kernel
	  vzh�adom na oper�ciu.
	- atrib�ty k-objektov. Dostupn� pomocou oper�tora '.'. Napr�klad:
	  process.uid
	- lok�lne premenn� definovan� pomocou kon�trukcie:
		local [<comm>] <typ k-objektu> <n�zov premennej>
	  <comm> je nepovinn� a predstavuje n�zov spojenia s kernelom, ktor�
	  or�uje k-objekt� ak�ho kernelu sa bud� bra� do �vahy. Uv�dza sa v
	  hranat�ch z�tvork�ch a uvodzovk�ch (napr�klad ["linux1"]). Ak nie je
	  uveden� berie sa kernel z ktor�ho iniciat�vy je t�to funkcia
	  vykon�vana. Platnos� premennej je v r�mci jednej funkcie, �i u�
	  obslu�nej funkcie udalosti, alebo pomocnej funkcie.
	- transparentn� premenn� definovan� kon�trukciou:
		transparent [<comm>] <typ k-objektu> <n�zov premennej>
	  S� podobn� ako lok�lne premenn� s t�m rozdielom, �e s� platn� aj
	  vo v�etk�ch podfunkci�ch ktor� su volan� z funkcie kde su definovan�.
	  Dostupn� s� v�dy priamo pod svojim n�zvom.
	- aliasy transparentn� a lok�lne definovan� kon�trukciou:
		transparent alias <existuj�ca premenn�> <nov� premenn�>
		local alias <existuj�ca premenn�> <nov� premenn�>
	  Takto vytvoren� nov� premenn� je odkazom na u� existuj�cu premennu.
	  Predstavuje duplicitn� n�zov pod ktor�m je pr�stupn� t� ist� premenn�.
	- argumenty funkcie dostupn� ako $<n>, kde <n> je poradov� ��slo
	  argumentu. Argumenty s� ��slovan� od 1. (teda $1 je prv� argument
	  funkcie)

	1.3.4.1. Platnost premenn�ch a skr�ten� formy z�pisu.

  Na atrib�ty k-objektov subjektu a objektu oper�cie, ako aj na atrib�ty
samotnej oper�cie sa d� odkazova� priamo ich menom bez nutnosti uv�dza� n�zov
subjektu, objektu, alebo oper�cie. (napr�klad na process.uid sa d� odk�za� aj
ako uid).

  Priorita premenn�ch:
	- ak je uveden� premenn� a atrib�t berie atrib�t oper�cie, subjektu
	  alebo objektu, ak sa n�zov premennej zhoduje s n�zvom oper�cie,
	  subjektu alebo objektu.
	- ak je uveden� premenn� a atrib�t berie atrib�t danej premennej platnej
	  v aktu�lnej funkcii.
	- ak je uveden� premenn� a atrib�t berie atrib�t danej transparentnej
	  premennej.
	- ak nie je uveden� premenn� a atrib�t berie sa atrib�t rovnak�ho mena
	  z oper�cie, subjektu alebo objektu oper�cie, ak existuje. Prioritn�
	  poradie je n�sleduj�ce:
		- oper�cia
		- subjekt
		- objekt
	- ak nie je uveden� premenn� a atrib�t berie sa k-objekt oper�cie,
	  subjektu alebo objektu, ak sa n�zov zhoduje.
	- ak nie je uveden� premenn� a atrib�t berie sa pemenn� platn�
	  v aktu�lnej funkcii.
	- ak nie je uveden� premenn� a atrib�t berie sa transparentn� premenn�.

	1.3.5. Vstavan� funkcie

  V programovacom jazyku s� dostupn� vstavan� funkcie.

	1.3.5.1. commof()

  commof( <v�raz> ) - vr�ti n�zov spojenia s kernelom zodpovedaj�ceho premennej
ur�enej v�razom <v�raz>.

	1.3.5.2. typeof()

  typeof( <v�raz> ) - vr�ti typ premennej ur�enej v�razom <v�raz>.

	1.3.5.3. fetch

  fetch <premenn�> - do premennej <premenn�> napln� inform�cie o k-objekte z
kernelu zodpovedaj�cemu commof(<premenn�>). K-objekt je identifikovan� k�u�ov�mi
atrib�tmi, ktor� musia by� vyplnen� pred volan�m tejto vstavanej funkcie.
K���ov� atrib�ty s� definovan� kernelom. Funkcia vr�ti logick� hodnotu
zodpovedaj�cu �spe�nosti naplnenia inform�ci� o k-objekte.

	1.3.5.4. update

  update <premenn�> - Nastav� inform�cie k-objektu v kerneli zodpovedaj�com
commof(<premenn�>) pod�a obsahu premennej. K-objekt je vyh�adan� pod�a k���ov�ch
atrib�tov. Funkcia vr�ti logick� hodnotu zodpovedaj�cu �spe�nosti nastavenia.

	1.3.5.5. constable_pid()

  constable_pid() - vr�ti PID be�iaceho constabla.

	1.3.5.6. hex()

  hex( <v�raz> ) - vr�ti textov� re�azec obsahuj�ci hexadecim�lny z�pis hodnoty
v�rzu <v�raz>, ktor� mus� by� signed, alebo unsigned integer.

	1.3.5.7. _comm()

  _comm() - vr�ti n�zov spojenia s kernelom z ktor�ho iniciat�vy je vykon�van�
t�to funkcia.

	1.3.5.8. _operation()

  _operation() - vr�ti aktu�lnu oper�ciu ako premenn�.

	1.3.5.9. _subject()

  _subject() - vr�ti subjekt aktu�lnej oper�cie ako premenn�.

	1.3.5.10. _object()

  _object() - vr�ti objekt aktu�lnej oper�cie ako premenn�.

	1.3.5.11. nameof()

  nameof( <v�raz> ) - vr�ti n�zov premennej zadanej v�razom <v�raz>.

	1.3.5.12. str2path()

  str2path( <v�raz> ) - vr�ti cestu v strome typu path. <v�raz> je textov�
re�azec popisuj�ci cestu. Tento preklad sa rob� a� po�as behu, preto ak je cesta
predom zn�ma, je vhodnej�ie pou�i� z�pis @"path". str2path() m��e zlyha� pri
pokuse o prevod cesty, ktor� nebola v celom konfigura�nom s�bore nikde uveden�
ako kon�tanta, alebo nebola pou�it� v defin�cii priestorov. V takomto pr�pade
funkcia vr�ti ��seln� hodnotu 0.

	1.3.5.13. spaces()

  spaces( <v�raz> ) - vr�ti textov� re�azec obsahuj�ci n�zvy priestorov
zodpovedaj�cich hodnote v�razu <v�raz>, ktor� musi by� typu bitmap.

	1.3.5.14. primaryspace()

  primaryspace( <kobject>, <path> ) - vr�ti textov� re�azec s n�zvom prim�rneho
priestoru k-objektu identifikovan�ho v�razom <kobject> v strome k-objektov
identifikovanom v�razom <path>, ktor� mus� by� typu path.

	1.3.5.15. enter()

  enter( <kobject>, <path> ) - explicitne zarad� k-objekt identifikovan�
v�razom <kobject> do stromu k-objektov do uzla ur�en�ho v�razom <path>, ktor�
mus� by� typu path.



-----

buildin funkcia strshl(string,n)  - zmaze prvych n znakov zo stringu a vrati novu dlzku stringu
buildin funkcia strcut(string,n)  - vysekne prvych n znakov zo stringu a vrati novu dlzku stringu
buildin funkcia sizeof(x) - vrati velkost x

defaultny kobject string atributy: string
defaultny kobject integer atributy: unsigned, signed  - union

